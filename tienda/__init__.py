__version__ = '0.1.0'
#!/usr/bin/env python3

from peewee import *
from playhouse.shortcuts import model_to_dict, dict_to_model
from flask import Flask, request, session, render_template, url_for, redirect
from flask_dotenv import DotEnv
import flask_admin as admin
from flask_admin import BaseView, expose
from flask_admin.contrib.peewee import ModelView
from flask_babelex import Babel

app = Flask(__name__)
env = DotEnv(app)
babel = Babel(app)
admin = admin.Admin(app, name='Tienda')

db = SqliteDatabase('tienda.db')

app.secret_key = '\xfd{H\xe5<\x95\xf9\xe3\x96.5\xd1\x01O<!\xd5\xa2\xa0\x9fR"\xa1\xa8'


class ModelBase(Model):
    class Meta:
        database = db

class Brand(ModelBase):
    name = CharField(verbose_name='Nombre')
    description = TextField(verbose_name='Descripcion')
    logo = CharField()
    instagram = CharField(null=True)
    facebook = CharField(null=True)
    twitter = CharField(null=True)

class Category(ModelBase):
    name = CharField(verbose_name='Nombre')
    parent = ForeignKeyField('self', null=True, verbose_name='Parent')

    def __str__(self):
        return self.name

class ProductType(ModelBase):
    name = CharField(verbose_name="Nombre")

    def __str__(self):
        return self.name

class Product(ModelBase):
    quantity = IntegerField(verbose_name='Cantidad')
    name = CharField(verbose_name='Nombre')
    description = TextField(verbose_name='Descripcion')
    price = IntegerField()
    category = ForeignKeyField(Category, backref="products", null=True)
    product_type = ForeignKeyField(ProductType, backref="products", verbose_name="Tipo de producto")

    def __str__(self):
        return self.name
        
class Feature(ModelBase):
    name = CharField(verbose_name='Nombre')
    product_type = ForeignKeyField(
        ProductType, 
        backref='features',
        null=True
    )

    def __str__(self):
        return self.name

class ProductFeature(ModelBase):
    product = ForeignKeyField(
        Product, 
        backref='features',
        verbose_name='Producto'
    )
    feature = ForeignKeyField(
        Feature, 
        backref='products',
        verbose_name='Caracteristica'
    )
    options = CharField(
        verbose_name='Opciones', 
        null=True,
        help_text='El cliente debe seleccionar una opcion. \
                    Usted debe agregarlos separado por comas, \
                    por ejemplo: Rojo, Negro, Blanco, Small, Large, XXL...',
    )
    value = CharField(null=True)

class Sale(ModelBase):
    user = CharField(verbose_name='Nombre del usuario')
    pay_money = BooleanField(null=True)
    pay_card = BooleanField(null=True)
    status = CharField()

class ProductSale(ModelBase):
    product = ForeignKeyField(Product, backref='sale')
    sale = ForeignKeyField(Sale, backref='product')

db.connect()
db.create_tables([
    ProductType,
    Product,
    Feature,
    ProductFeature,
    Category,
    Sale,
    ProductSale,
    Brand
])

class ProductAdmin(ModelView):
    column_searchable_list = ['name']

admin.add_view(ModelView(Brand, name='Marca'))
admin.add_view(ModelView(ProductType, name='Tipos de producto', category="Productos"))
admin.add_view(ProductAdmin(Product, name='Productos', category="Productos"))
admin.add_view(ModelView(Feature, name='Caracteristicas Generales', category="Productos"))
admin.add_view(ModelView(ProductFeature, name='Caracteristicas por Producto', category="Productos"))
admin.add_view(ModelView(Category, name='Categorias', category="Productos"))
admin.add_view(ModelView(Sale, name='Ventas'))
admin.add_view(ModelView(ProductSale, name='Productos en venta'))

@babel.localeselector
def get_locale():
    if request.args.get('lang'):
        session['lang'] = request.args.get('lang')
    return session.get('lang', 'es')

@app.route('/')
def index():
    # return render_template('index.html')
    return redirect('/admin')

if __name__ == '__main__':
    app.run(debug=True)