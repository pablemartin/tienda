===
Tienda Digital
===

Desarrollo tipo e-commerce con Flask y Peewee.

TODO

- [ ] Modelos Base: productos, categorias, carrito, pedidos, ventas.
- [ ] Vistas de: principal, catalogo, productos, carrito, pedido.
- [ ] Transaccion con tarjetas.
- [ ] Autenticacion.
- [ ] Template demostracion.
